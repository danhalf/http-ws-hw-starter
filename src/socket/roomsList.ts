const rooms = {} as any;
const users = [] as any;



export default (io) => {

  const deleteEmptyRoom = (roomId): void => {
    io.in(roomId).socketsLeave(roomId);
    for (let key in rooms) {
      if (!rooms[key].usersInRoom.length) delete rooms[key]
    }
    io.emit('UPDATE_ROOMS', rooms);
  }


  io.on('connection', (socket) => {
    const userName = socket.handshake.query.username;
    if (!users.includes(userName)) {
      users.push(userName);
    } else {
      return socket.emit('USERNAME_ALREADY_TAKEN', userName)
    }

    socket.on('CREATE_ROOM', (roomName) => {
      rooms[roomName] = { usersInRoom: [userName] };
      io.emit('UPDATE_ROOMS', rooms);
    });

    socket.on('JOIN_ROOM', (roomId) => {
      if (rooms[roomId]?.usersInRoom.includes(userName)) {
        return
      }
      rooms[roomId]?.usersInRoom.push(userName);

      socket.join(roomId, () => {
        io.to(socket.id).emit('JOIN_ROOM_DONE', { roomId });
      });

      socket.emit('UPDATE_ROOMS', rooms);
    });


    socket.on('LEAVE_ROOM', (roomId) => {
      if (rooms[roomId]?.usersInRoom) {
        const currentUsers = [...rooms[roomId].usersInRoom];
        const updatedCurrentUsers = currentUsers.filter(user => user !== userName);
        rooms[roomId].usersInRoom.length = 0;
        rooms[roomId].usersInRoom.push(...updatedCurrentUsers);
        socket.emit('UPDATE_ROOMS', rooms);
        deleteEmptyRoom(roomId)
      }

    })

    socket.on('disconnect', () => {
      const updatedUsers = users.filter((user) => user !== userName);
      users.length = 0;
      users.push(...updatedUsers);
      io.emit('UPDATE_USERS', users);
    });

    io.emit('UPDATE_USERS', users);
    socket.emit('UPDATE_ROOMS', rooms);
  });
};
