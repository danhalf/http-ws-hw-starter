import { Server } from 'socket.io';
import * as config from './config';
import roomsList from "./roomsList";

export default (io: Server) => {
	io.on('connection', socket => {
		const username = socket.handshake.query.username;
		console.log(`id: ${socket.id}, user: ${username} connection`);
	});

	roomsList(io.of("/rooms-list"));

	// io.on('disconnect', socket => {
	// 	console.log(`${socket.id} has been offline`)
	// })
};
