import { addClass, createElement, removeClass } from './helpers/domHelper.mjs';
import { appendRoomElement } from './views/room.mjs';
import { appendUserElement, removeUserElement } from './views/user.mjs';
import { showMessageModal } from "./views/modal.mjs";

const username = sessionStorage.getItem('username');
const roomname = sessionStorage.getItem('roomname')

const goToLoginPage = (user) => {
  const returnToLogin = () => {
    window.sessionStorage.clear();
    window.location.replace("/login");
  }
  showMessageModal({message: `name ${user} already taken`, onClose: returnToLogin})
  setInterval(returnToLogin, 3000)
}

const socket = io('http://localhost:3002/rooms-list', { query: { username } });



if (!username) {
  socket.close();
  window.location.replace('/login');
}


const gamePage = document.getElementById('game-page');
const roomsPage = document.getElementById('rooms-page');
const roomName = document.getElementById('room-name');
const modal = document.querySelector('.room-modal');
const modalOpenButton = document.getElementById('add-room-btn');
const modalCloseButton = document.getElementById('modal-room-close-btn');
const modalInput = document.getElementById('modal-room-input');
const startGameBtn = document.getElementById('modal-room-btn');
const roomsContainer = document.getElementById('rooms-wrapper');
const quitRoomButton = document.getElementById('quit-room-btn');

let activeRoomId = null;


const setActiveRoomId = ({ roomId }) => {
  activeRoomId = roomId;
};

const openModal = () => {
  addClass(modal, 'd-block');
};

const closeModal = () => {
  removeClass(modal, 'd-block');
};

const startGame = (roomInfo) => {
  const [roomId, usersInRoom] = roomInfo;
  const onJoinRoom = () => {
    joinRoom(roomId)
  };



  appendRoomElement({
    name: roomId,
    numberOfUsers: usersInRoom?.length,
    onJoin: onJoinRoom,
  });

  const room = document.querySelector(`.room[data-room-name='${roomId}']`);
  const roomName = room.querySelector('.room-name');
  room.addEventListener('click', onJoinRoom);

  roomName.innerText = roomId;

  return room;
};

const joinRoom = (roomId) => {
  setActiveRoomId(roomId)
  sessionStorage.setItem('roomname', roomId)
  removeClass(gamePage, 'display-none');
  addClass(roomsPage, 'display-none');
  roomName.innerHTML = roomId;
  socket.emit('JOIN_ROOM', roomId);
};

const leaveRoom = () => {
  addClass(gamePage, 'display-none');
  removeClass(roomsPage, 'display-none');
  sessionStorage.removeItem('roomname')
  removeUserElement(username)
  socket.emit('LEAVE_ROOM', roomname)
}

if (username && roomname) {
  joinRoom(roomname)
}


const addRoom = () => {
  const inputValue = modalInput.value;
  if (!inputValue) {
    modalInput.style.borderColor = 'red';
    return;
  }
  socket.emit('CREATE_ROOM', inputValue);
  const currentRooms = document.querySelectorAll('.room');
  const currentRoomsNames = [];
  currentRooms.forEach((room) => currentRoomsNames.push(room.textContent));
  updateRooms(currentRoomsNames);
  joinRoom(inputValue)
};



const updateRooms = (rooms) => {
  const roomsInfo = [];
  for (let key in rooms) {
    roomsInfo.push([key, rooms[key].usersInRoom]);
    if(key === roomname) {
      rooms[key].usersInRoom.map(user => {
        const isCurrentUser = user === username
        appendUserElement({ username: user, ready: false, isCurrentUser })
      })
    }

  }
  const allRooms = roomsInfo.map(startGame);
  roomsContainer.innerHTML = '';
  roomsContainer.append(...allRooms);
};

const updateUsers = (users) => {
  const allUsers = document.getElementById('all-users');
  allUsers.innerHTML = '';
  const userItems = users.map(
    (user) => `<span style='margin: 5px'>${user}</span>`
  );
  allUsers.insertAdjacentHTML('beforeend', userItems.join(''));
};

const joinRoomDone = ({ roomId }) => {
  const newRoomElement = document.getElementById(roomId);
  addClass(newRoomElement, 'active');

  if (activeRoomId) {
    const previousRoomElement = document.getElementById(activeRoomId);
    removeClass(previousRoomElement, 'active');
  }

  setActiveRoomId(roomId);
};

modalOpenButton.addEventListener('click', openModal);
modalCloseButton.addEventListener('click', closeModal);
startGameBtn.addEventListener('click', addRoom);
quitRoomButton.addEventListener('click', leaveRoom);

socket.on('USERNAME_ALREADY_TAKEN', goToLoginPage)
socket.on('UPDATE_ROOMS', updateRooms);
socket.on('UPDATE_USERS', updateUsers);
socket.on('JOIN_ROOM_DONE', joinRoomDone);
